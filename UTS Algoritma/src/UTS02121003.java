import java.util.Scanner;
public class UTS02121003 {
    public static void main(String[] args) {
        String npm,nama;
        Scanner sc = new Scanner(System.in);
        System.out.print("Silahkan masukkan NPM Anda: ");
        npm = sc.nextLine();
        System.out.println("NPM Anda: "+npm);
        System.out.print("Silahkan masukkan Nama Anda: ");
        nama = sc.nextLine();
        System.out.println("Nama Anda: "+nama);
        String result = null;
        System.out.print("Silahkan masukkan nilai: ");
        int nilai = sc.nextInt();
        System.out.println("Nilai Anda adalah "+nilai);
        if(nilai>=70){
            result = "Lulus";
        }else if(nilai<70){
            result = "Tidak Lulus";
        }
        System.out.println("Hasil Ujian Anda "+result);
    }
}
